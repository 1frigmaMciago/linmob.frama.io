+++
title = "Weekly GNU-like Mobile Linux Update (2/2023): A Maemo Leste report"
date = "2023-01-15T22:10:00Z"
draft = false
[taxonomies]
tags = ["Maemo Leste", "Sailfish OS", "PinePhone", "Phosh", "PineNote","Nheko", "phoc",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Sailfish OS on PinePhone gets a camera app, a new release of biktorgj's PinePhone Modem Firmware, Phoc 0.23.0, and Nheko 0.11.0.
<!-- more -->

_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#78 Volume Levels](https://thisweek.gnome.org/posts/2023/01/twig-78/)
- Tobias Bernard: [Post Collapse Computing Part 4: The Road Ahead](https://blogs.gnome.org/tbernard/2023/01/10/post-collapse-computing-4/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Well just look at all these pictures!](https://pointieststick.com/2023/01/13/this-week-in-kde-well-just-look-at-all-these-pictures/)
- cordlandwehr: [Running Plasma on VisionFive-2](https://cordlandwehr.wordpress.com/2023/01/14/running-plasma-on-visionfive-2/)
- KDE Announcements: [KDE Ships Frameworks 5.102.0](https://kde.org/announcements/frameworks/5/5.102.0/)
- dot.KDE.org: [Akademy 2023 will be held in Greece](https://dot.kde.org/2023/01/12/akademy-2023-will-be-held-greece)

#### Sailfish OS
- [Sailfish Community News, 12th January, Privacy and Control](https://forum.sailfishos.org/t/sailfish-community-news-12th-january-privacy-and-control/14063)
- [Adam Pigg (@adampigg): "Beginnings of a #sailfishos camera app 'pinhole' for the @thepine64 #pinephone built on libcamera"](https://twitter.com/adampigg/status/1613948530649104386#m)

#### Distributions
- Breaking updates in pmOS edge: [SDM845 no audio on Linux 6.1.3](https://postmarketos.org/edge/2023/01/11/sdm845-audio/)
- glodroid_manifest (GitHub): [GloDroid v0.8.2](https://github.com/GloDroid/glodroid_manifest/releases/tag/v0.8.2)
- [Gerben Jan Dijkman: "I've found out that I made an "huge" error in my PinePhone Pro Documentation. …"](https://hub.gjdwebserver.nl/@gjdijkman/109666754907404443)

#### PinePhone Modem Firmware
- biktorgj Modem Firmware: [0.7.2: Tell me lies, tell me sweet little lies](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.2). _Stuck messages have been ruining some peoples experience, so this is a big release!_

#### Stack
- Phoronix: [Mesa 23.0 Feature Development Ends With Many Vulkan Additions](https://www.phoronix.com/news/Mesa-23.0-Branched)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-01-13](https://matrix.org/blog/2023/01/13/this-week-in-matrix-2023-01-13)
- nheko (Matrix): [v0.11.0](https://github.com/Nheko-Reborn/nheko/releases/tag/v0.11.0)

### Worth noting
- megi's PinePhone Development Log: [Pinephone cpuidle](https://xnux.eu/log/#077)
    - related postmarketOS Issue: [cpuidle broken on pine64-pinephone (#1901)](https://gitlab.com/postmarketOS/pmaports/-/issues/1901)
- [Guido Günther: "As the last component of the 0.23.0 release set #phoc 0.23.0 is out, fixing the pixman renderer to help #CI testing, running in #qemu and platforms that can't or don't want to use GLES2. If you don't need any of these it's safe to wait for 0.24.0. …"](https://social.librem.one/@agx/109687813175900231)
- [Guido Günther: "Some progress to handle notches / cutouts of #linuxmobile phones better in #phosh…" - Librem Social](https://social.librem.one/@agx/109682923272149132)
- [Mobian: "Knock knock, who's there? An *experimental* kernel package that works on both the OG #pinephone AND the #pinephonepro !…"](https://fosstodon.org/@mobian/109686820116653147). _Awesome!_

### Worth reading
- Camden Bruce: [Nearly three years with the PinePhone.](https://medium.com/@camden.o.b/nearly-three-years-with-the-pinephone-8e0bd427f776)
- Maemo Leste: [Maemo Leste - New Year update: May 2022 - January 2023](https://maemo-leste.github.io/maemo-leste-new-year-update-may-2022-january-2023.html)
- Purism: [Phosh 2022 in Retrospect](https://puri.sm/posts/phosh-2022-in-retrospect/). _Yesterday, I used a "pre-gesture release" of Phosh and wow, 2022 was a year of massive progress for Phosh!_ 
- Purism: [Spreading Awareness about Purism in 2022](https://puri.sm/posts/spreading-awareness-about-purism-in-2022/)
- Alberto Mardegan: [Back to Maemo!](https://www.mardy.it/blog/2023/01/back-to-maemo.html)

### Worth watching
- Nicco Loves Linux: [KDE on the Linux E-INK Tablet!](https://www.youtube.com/watch?v=sOLD5YUNz04) _PineNote!_
- The Linux Experiment: [THIS is why I'm LEAVING ANDROID](https://www.youtube.com/watch?v=Lt2xg5NXgmU)
- Lup Yuen Lee: [#LVGL on #PinePhone on Apache #NuttX RTOS ... Increased the Default Font Size from 14 to 20 🤔](https://www.youtube.com/watch?v=N-Yc2jj3TtQ)
- Lup Yuen Lee: [#LVGL on #PinePhone with Apache #NuttX RTOS ... Works much better now! Always use lv_port_tick 👍](https://www.youtube.com/watch?v=APge9bTt-ho)
- Lup Yuen Lee: [Tweaking #LVGL Settings for #PinePhone on Apache #NuttX RTOS ... To make it more Touch-Friendly 🤔](https://www.youtube.com/watch?v=De5ZehlIka8)
- Linux Stuff: [Playing Cuphead on the Juno Tablet](https://www.youtube.com/watch?v=ticPVqbdLwc)
- Scientific Perspective: [PostmarketOS inside chroot | Ubuntu Touch #shorts](https://www.youtube.com/watch?v=VWYbbxCzbNM)
- Repair A2Z: [Ubuntu Touch Linux OS Redmi4x | How to install Ubuntu Touch #linuxOS #UbuntuTouch](https://www.youtube.com/watch?v=mU3XuJ5gw0o)
- Continuum Gaming: [Microsoft Continuum Gaming E347: 3 (sort of) puzzling games](https://www.youtube.com/watch?v=ymsQmhm7ETE)
- DHOCNET: [PostmarketOS Native Linux Running On Xiaomi Redmi 2 Prime | wt88047 2GB of RAM](https://www.youtube.com/watch?v=5nJCBomy7Ms)
- Twinntech: [Pinephone is a CULT](https://www.youtube.com/watch?v=HaoGHkhJH2U) 

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

