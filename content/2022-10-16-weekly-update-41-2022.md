+++
title = "Weekly GNU-like Mobile Linux Update (41/2022): Plasma 5.26 and SGX 540 reverse engineering"
date = "2022-10-16T19:50:00Z"
draft = false
[taxonomies]
tags = ["PINE64 Community Update","postmarketOS","Plasma Mobile",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++
Also: A Pi Phone, Ox64, better battery life for the Librem 5 and verification on Flathub beta.
<!-- more -->
_Commentary in italics._

### Hardware
- Hackaday: [A Raspberry Pi Phone For The Modern Era](https://hackaday.com/2022/10/10/a-raspberry-pi-phone-for-the-modern-era/). _Nice build!_

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#65 Officially Deprecated](https://thisweek.gnome.org/posts/2022/10/twig-65/)
- Will Thomson: [GNOME 43: Endless’s Part In Its Creation](https://blogs.gnome.org/wjjt/2022/10/13/gnome-43-endlesss-part-in-its-creation/)

#### Plasma/Maui ecosystem
- Nate Graham: [This week in KDE: QA pays off](https://pointieststick.com/2022/10/14/this-week-in-kde-qa-pays-off/)
- KDE Announcements: [Plasma 5.26](https://kde.org/announcements/plasma/5/5.26.0/)
- KDE Announcements: [KDE Gear 22.08.2](https://kde.org/announcements/gear/22.08.2/)
- KDE Announcements: [KDE Ships Frameworks 5.99.0](https://kde.org/announcements/frameworks/5/5.99.0/)
- Volker Krause: [KDE Frameworks 6 Windows CI and Branching Plan](https://www.volkerkrause.eu/2022/10/14/kf6-windows-ci-branching-plan.html)
- Qt blog: [Qt Creator 9 Beta released](https://www.qt.io/blog/qt-creator-9-beta-released)
- Mardi: [MiTubo 1.4 adds feed folders](http://mardy.it/blog/2022/10/mitubo-14-adds-feed-folders.html)

#### Distributions
- Breaking updates in pmOS edge: [Waydroid: pmOS image deleted, requires user intervention](https://postmarketos.org/edge/2022/10/14/waydroid/)
- Breaking updates in pmOS edge: [GNOME Shell on mobile breakage](https://postmarketos.org/edge/2022/10/11/gnome-shell-on-mobile-regression/)
- Breaking updates in pmOS edge: [Xfce4: postmarketOS icon might disappear](https://postmarketos.org/edge/2022/10/10/xfce4-postmarketOS-icon-might-disappear/)
- Dan Johansen: [Plasma 5.26 and Plasma Mobile Gear 22.09 in Manjaro ARM](https://blog.strits.dk/plasma-5-26-and-plasma-mobile-gear-22-09/)
- Phoronix: [Debian 14 Codenamed "Forky"](https://www.phoronix.com/news/Debian-14-Forky)

#### Non-Linux
- Phoronix: [Genode's Sculpt OS 22.10 Brings Performance Optimizations, Better USB Hotplug](https://www.phoronix.com/news/Sculpt-OS-22.10)

#### Kernel
- Phoronix: [Linux 6.0.2, 5.19.16 & Other Point Releases Arrive For Fixing WiFi Stack Vulnerabilities](https://www.phoronix.com/news/Linux-6.0.2-Point-Releases-WiFi)
- Phoronix: [Linux Gets Patched For WiFi Vulnerabilities That Can Be Exploited By Malicious Packets](https://www.phoronix.com/news/Linux-WiFi-Malicious-Packets)

#### Matrix
- matrix.org: [This Week in Matrix 2022-10-14](https://matrix.org/blog/2022/10/14/this-week-in-matrix-2022-10-14)

### Worth noting
- [Linux 6.0 should improve Librem 5 battery life](https://forums.puri.sm/t/linux-6-x-kernel/18422/4)
- Flathub [implementing "Verified" icon](https://github.com/flathub/website/pull/734) on beta site.

### Worth reading
* Garnet: [Reverse Engineering a GPU from 2009](https://garnet.codeberg.page/posts/gpu-reversing/) *the comprehensive story behind the [PowerVR SGX540 reversing](https://codeberg.org/Garnet/sgx540-reversing) efforts mentioned in weekly update 38*
- PINE64: [October update: An Ox, no bull](https://www.pine64.org/2022/10/15/october-update-an-ox-no-bull/)
  - @thanos_engine: ["How do y'all feel about making a smart-ish-phone based around the BL808? I'm thinking that could be a pretty neat open source project to get started but it's well beyond my skill level on the software side so I'd need some help there."](https://twitter.com/thanos_engine/status/1579276649627267072)
  - Robert Lipe: [First thoughts on the (a)symmetery of Bouffalo Labs BL808 as in Pine64’s Ox64](https://www.robertlipe.com/bl808-not-symmetric/)
- FossPhones.com: [Linux Phone News - October 11, 2022](https://fossphones.com/10-11-22.html). _If rav3ndust keeps this up, I can finally retire!_
- TuxPhones.com: [An unexpected revival of Firefox OS](https://tuxphones.com/capyloon-firefox-os-b2gos-linux/). _It's fun to get replies on months old tweets!_
- Martijn Braam: [Automated Phone Testing pt.4](https://blog.brixit.nl/automated-phone-testing-pt-4/)
- Duncan Bayne: [Back to Android (for the time being)](gemini://duncan.bayne.id.au/gemlog/back-to-android/), [web mirror](https://portal.mozz.us/gemini/duncan.bayne.id.au/gemlog/back-to-android/). 
  - Comment threads: [Peanut gallery](https://news.ycombinator.com/item?id=33150362), [r/peanutgallery](https://www.reddit.com/r/linux/comments/y0ehn2/back_to_android_for_the_time_being_sadly_my/), [Peanut Gallery you cannot easily sign up to](https://lobste.rs/s/94avol/back_android_for_time_being).
- Tobias Bernard: [Post Collapse Computing Part 2: What if we Fail?](https://blogs.gnome.org/tbernard/2022/10/10/post-collapse-computing-2/)
- TheEvilSkeleton: [How I Started Programming, and How You Can Too](https://theevilskeleton.gitlab.io/2022/10/10/how-I-started-programming-and-how-you-can-too.html)

### Worth listening
- postmarketOS Podcast: [#23 PVR SGX540, Kupfer, UBports installer, SoCs, Phone Testing II](https://cast.postmarketos.org/episode/23-PVR-SGX540-Kupfer-UBports-installer-SoCs-Phone-Testing-2/)

### Worth watching
- Phalio: [PinePhone Keyboard Keycaps Getting Stuck](https://tube.tchncs.de/w/uUsHevss2P7fTjBiPUz5ts)
- Phalio: [Inserting the PinePhone into the Keyboard Case](https://tube.tchncs.de/w/vHbny4a8uAEw133JzM2XWK)
- Jacob David Cunningham: [Light web dev with docked Pinephone Pro](https://www.youtube.com/watch?v=VlTxw400gcc)
- Jacob David Cunningham: [Messing around with Pinephone Pro](https://www.youtube.com/watch?v=cdU23S34uoM)
- The Gibus Cap: [PinePhone Keyboard Typing](https://www.youtube.com/watch?v=gQIzh9qOj9I)
- Equareo: [Fairphone 4 in desktop mode on ubuntu touch](https://www.youtube.com/watch?v=BozwDAuM7VY)
- my Linux: [How to install flash Mobian on to your PinePhone!](https://www.youtube.com/watch?v=66OrpoxsiRw)
- Scientific Perspective: [Ubuntu Touch OTA devel update alongside AOSP | Multirom | Omnirom boot | POCO F1](https://www.youtube.com/watch?v=VRbGM4eyzo8)
- U STARK: [Droidian](https://www.youtube.com/watch?v=JxUar-OmZNk)
- Scientific Perspective: [Ubuntu Touch OTA update alongside AOSP | Multirom | POCO F1 #shorts](https://www.youtube.com/watch?v=G1QXn8RpzmM)
- Random Repairs: [Sailfish OS for Xiaomi MI4/3 - Not Android!](https://www.youtube.com/watch?v=XYtDyRE_glU)
- Continuum Gaming: [Microsoft Continuum Gaming E333: Traffic Data in PureMaps for Sailfish OS](https://www.youtube.com/watch?v=1_YnIHlqgb0)
- caleb: [(Re)writing an upstreamable haptics driver for Snapdragon Linux Phones](https://www.youtube.com/watch?v=BtF5E8cBeJc)
- KDE: [Plasma 5.26 is All About the Widgets](https://tube.kockatoo.org/w/iw8XnpcPVpWVTxbVmGyBv3)

### Thanks

Huge thanks to

* [hamblingreen,](https://hamblingreen.com) *whose new website might be working now*
* [nikodunk](https://nikodunk.com), and
* ollieparanoid, *just had to add that excellent SGX540 blog post*

and possible further anonymous contributors and also to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot. 

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)
PPS: This one has less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)

