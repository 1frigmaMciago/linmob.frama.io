+++
title = "Weekly #LinuxPhone Update (19/2022): OpenSUSE on Volla Phone,  Phone calls on Nemo Mobile with a caveat, and another Modem Firmware release!"
date = "2022-05-15T14:20:00Z"
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Waydroid","Maui Shell","Lomiri","Librem 5 USA"]
categories = ["weekly update"]
authors = ["peter"]
+++
 
It's the 15th, but [PINE64 had to delay their community update](https://twitter.com/LukaszErecinsk1/status/1523952982118834176). So let's at least get this weekly roundup out! _It's a bit boring though._
<!-- more -->

_Commentary in italics._

#### Firmware
* biktorgj published [release 0.6.5](https://github.com/biktorgj/pinephone_modem_sdk/releases/tag/0.6.5) of their open source firmware for the PinePhone (Pro) modem! _Call +22 33 44 55 66 77 now to talk to your modem! On a more serious note: Do read the release notes, there have been more changes than the small version bump may suggest!_

### Software progress
#### GNOME ecosystem
* This Week in GNOME: [#44 Foundation News](https://thisweek.gnome.org/posts/2022/05/twig-43/). 
* chergert: [Builder GTK 4 Porting, Part IV](https://blogs.gnome.org/chergert/2022/05/14/builder-gtk-4-porting-part-iv/).
* Marcus Lundblad: [Maps Spring Cleaning](http://ml4711.blogspot.com/2022/05/maps-spring-cleaning.html).
* oleaamot: [Voice](https://blogs.gnome.org/oleaamot/2022/05/14/gnome-voice/). _No, I don't know what a Voicegram is either._
* Sophie Herold: [Pika Backup 0.4 Released with Schedule Support](https://blogs.gnome.org/sophieh/2022/05/15/pika-backup-0-4-released-with-schedule-support/).

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: something for everyone](https://pointieststick.com/2022/05/13/this-week-in-kde-something-for-everyone/).
* Volker Krause: [Building Plasma for KDE Frameworks 6](https://www.volkerkrause.eu/2022/05/14/kf6-plasma-build-progress.html).
* KDE Announcements: [KDE Gear 22.04.1](https://kde.org/announcements/gear/22.04.1/). _Just a bugfix release for KDE's apps!_
* KDE Announcements: [KDE Ships Frameworks 5.94.0](https://kde.org/announcements/frameworks/5/5.94.0/). 
* Albert Astals Cid: [The KDE Qt5 Patch Collection has been rebased on top of Qt 5.15.4](https://tsdgeos.blogspot.com/2022/05/the-kde-qt5-patch-collection-has-been.html). _Thanking the Commercial users for beta testing is a nice touch!_

#### Distro news
* Having audio issues on DanctNIX? [They've posted instructions that solved the issue for me](https://fosstodon.org/web/@danctnix/108298691019946344).
* If all you ever wanted is to run Plasma Mobile on Mobian, [have at it](https://wiki.mobian.org/doku.php?id=desktopenvironments#plasma)!
* OpenSUSE's mobile effort supports an additional, halium based device: [The Volla Phone](https://twitter.com/hadrianweb/status/1524449265103245312). (via [gnulinux.ch](https://gnulinux.ch/opensuse-tumbleweed-v20220511-fuer-das-volla-phone)) _Too bad I handed my compatible device to my brother! Although, considering my full schedule... it's alright. Let me know how it's going if you've tried this!_ 
* As you all know, postmarketOS [support many UIs](https://wiki.postmarketos.org/wiki/Category:Interface). [Now, it looks like Lomiri (of Ubuntu Touch fame) is going to be another option soon!](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3123) ([Thanks, caleb](https://fosstodon.org/@calebccff/108289639207111454)!)

#### Kernel
* Phoronix: [Rockchip VOP2 DRM Driver Coming To Linux 5.19 For Display Support With Newer SoCs](https://www.phoronix.com/scan.php?page=news_item&px=Rockchip-VOP2-Linux-5.19). _While this does not affect the chips in current Linux Phones, it's good news for future Linux Phones!_

#### Non-Linux
* Genodians: [Pine fun - Telephony (Roger, Roger?)](https://genodians.org/ssumpf/2022-05-09-telephony). _Fun read!_


#### Worth noting
* According [to reddit user /u/The01player using the LoRaWAN backcover can badly drain the PinePhone's battery](https://teddit.net/r/PINE64official/comments/ums0z3/psa_the_lora_pinephone_case_seems_to_drain/).
* Use Manjaro? Want Waydroid on your PinePhone Pro? Check out [Manjaro Waydroid Manager](https://github.com/MadameMalady/Manjaro-Waydroid-Manager) by [@FOSSingularity](https://twitter.com/FOSSingularity)!
* Also, by the same developer: [A script to try Maui Shell on Manjaro](https://github.com/MadameMalady/PinePhone-Pro-Manjaro-Maui-Shell-Installer-MMSI)!


### Worth reading

#### Password management
* hamblinggreen: [My Love-Hate Relationship With Bitwarden](https://hamblingreen.gitlab.io/2022/05/07/my-love-hate-relationship-with-bitwarden.html). _This would have been one for last week, but I did not skim it carefully enough. See also: [Pass UIs](https://linuxphoneapps.org/backends/pass/)._ 

#### Librem 5 
* Purism: [Improved Delivery Time for Librem 5 USA: May 2022 Update](https://puri.sm/posts/improved-delivery-time-for-librem-5-usa-may-2022-update/). _The [comment thread](https://forums.puri.sm/t/new-post-improved-delivery-time-for-librem-5-usa-may-2022-update/17236) on Purism's forums is the usual.
* The Arcology Garden: [First Impression of the Purism Librem5](https://arcology.garden/librem5-first-impressions). _Great write up!_

### Worth listening
* Linux Lads: [Season 7 - Episode 8: Sailing New Waters](https://linuxlads.com/episodes/season-7-episode-8/). _Featuring Adam Pigg, Sailfish OS enthusiast and porter (e.g. PINE64 devices)._

### Worth watching

#### Sxmo
* Bosco Vallejo-Nágera: [Pinephone with sxmo (wayland) - thoughts after ~1 month of usage](https://www.youtube.com/watch?v=u1KWHSp41BY). _Awesome in-depth video of Sxmo!_

#### Nemo Mobile
* Jozef Mlich: [Nemomobile: phone call with PinePhone](https://www.youtube.com/watch?v=M7z-WVSs0Rg). _Ok, audio does not work yet, but it's still an achievement!_

#### Ubuntu Touch
* Phonez Pop: [Poco X3 Pro Running UBUNTU Touch 🔥 🔥 🔥 Features, First looks](https://www.youtube.com/watch?v=HaoHBBLMups).

#### Keyboarded handhelds
* mutantC: [Sony Vaio UX UMPC review with Debian Linux](https://www.youtube.com/watch?v=SFnMh2CevHU).

#### Shorts
* Baonks81: [Ubuntu MATE 22.04 LTS Nexus 7 2012 grouper mainline kernel-5.15.0-rc4-postmarketOS-grate](https://www.youtube.com/shorts/it-JM-GkJxw).

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
